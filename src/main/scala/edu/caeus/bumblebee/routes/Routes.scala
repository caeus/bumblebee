package edu.caeus.bumblebee.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{ExceptionHandler, Route}
import edu.caeus.bumblebee.engines.{ChargeSessionsEngine, TariffsEngine}
import edu.caeus.bumblebee.errors.{ErrorMessage, ManagedException}
import edu.caeus.bumblebee.models.{ChargeSessionSeed, TariffSeed}
import edu.caeus.bumblebee.util.akka.UpickleSupport._
import edu.caeus.bumblebee.util.json.Codecs._

object Routes {

  def exceptionHandler = ExceptionHandler {
    case me: ManagedException =>
      complete(me.httpStatus, ErrorMessage(me.message, me.details))
    case e =>
      complete(StatusCodes.InternalServerError,
               ErrorMessage(s"Internal server error: ${e.getMessage}", Nil))
  }

  def apply(tariffsEngine: TariffsEngine,
            chargeSessionsEngine: ChargeSessionsEngine): Route = {

    pathPrefix("v1") {
      pathPrefix("tariffs") {
        (pathEndOrSingleSlash & post) {
          // handleWith could've been used, but it hides a lot of implementation details with implicits and becomes
          // unreadable
          entity(as[TariffSeed]) { seed =>
            complete(tariffsEngine.set(seed))
          }
        }
      } ~
        pathPrefix("charge-sessions") {
          (pathEndOrSingleSlash & post) {
            entity(as[ChargeSessionSeed]) { seed =>
              complete(chargeSessionsEngine.add(seed))
            }
          } ~
            (path(Segment) & get) { customerId =>
              complete(chargeSessionsEngine.all(customerId))
            }
        }
    }

  }

}

package edu.caeus.bumblebee.models

import java.util.Currency

import edu.caeus.bumblebee.util.json.Codecs._

case class Price(value: Double, currency: Currency) {

  def *(scalar: Double): Price = {
    Price(value * scalar, currency)
  }
}
object Price{
  implicit def readwriter:ReadWriter[Price]=macroRW
}

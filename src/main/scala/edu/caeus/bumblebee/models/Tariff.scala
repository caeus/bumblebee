
package edu.caeus.bumblebee.models

import java.time.ZonedDateTime
import java.util.Currency

import edu.caeus.bumblebee.util.json.Codecs._

/**
  * I assume the semantic of the optional values is to replace the previous value
  */
case class TariffSeed(
                       currency: Currency,
                       startFee: Option[Double] = None,
                       hourlyFee: Option[Double] = None,
                       feePerKWh: Option[Double] = None,
                       activeStarting: ZonedDateTime
                     )

object TariffSeed {

  implicit def readwriter: ReadWriter[TariffSeed] = macroRW
}

case class Tariff(
                   startFee: Price,
                   hourlyFee: Price,
                   feePerKWh: Price,
                   activeSince: ZonedDateTime) {

  def update(seed: TariffSeed): Tariff = {

    Tariff(
      startFee = seed.startFee.map {
        value => Price(value, seed.currency)
      }.getOrElse(startFee),
      hourlyFee = seed.hourlyFee.map {
        value => Price(value, seed.currency)
      }.getOrElse(hourlyFee),
      feePerKWh = seed.feePerKWh.map {
        value => Price(value, seed.currency)
      }.getOrElse(feePerKWh),
      activeSince = seed.activeStarting
    )
  }

}

object Tariff {
  implicit def readwriter: ReadWriter[Tariff] = macroRW
}
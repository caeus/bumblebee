package edu.caeus.bumblebee.models

import java.time.ZonedDateTime

import edu.caeus.bumblebee.util.json.Codecs._

case class ChargeSessionSeed(customerId: String,
                             startTime: ZonedDateTime,
                             endTime: ZonedDateTime,
                             volume: Double)

object ChargeSessionSeed {
  implicit def reader: Reader[ChargeSessionSeed] = macroR
}

case class ChargeSession(customerId: String,
                         startTime: ZonedDateTime,
                         endTime: ZonedDateTime,
                         volume: Double,
                         price: Seq[Price])

object ChargeSession {
  implicit def readwriter: ReadWriter[ChargeSession] = macroRW
}
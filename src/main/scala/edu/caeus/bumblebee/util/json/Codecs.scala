package edu.caeus.bumblebee.util.json

import java.time.ZonedDateTime
import java.util.Currency

object Codecs extends upickle.AttributeTagged {

  implicit def zonedDateTimeReadWriter: ReadWriter[ZonedDateTime] = readwriter[String]
    .bimap[ZonedDateTime](_.toString, ZonedDateTime.parse)

  implicit def currencyReadWriter: ReadWriter[Currency] = readwriter[String]
    .bimap[Currency](_.getCurrencyCode, Currency.getInstance)


  // Had to add this because upickle treats options as json arrays with one or no elements
  // That's pretty weird
  override implicit def OptionWriter[T: Writer]: Writer[Option[T]] =
    implicitly[Writer[T]].comap[Option[T]] {
      case None => null.asInstanceOf[T]
      case Some(x) => x
    }

  override implicit def OptionReader[T: Reader]: Reader[Option[T]] =
    implicitly[Reader[T]].mapNulls {
      case null => None
      case x => Some(x)
    }

}

package edu.caeus.bumblebee.util.syntax

import akka.actor.{ActorRef, Status}

import scala.util.{Failure, Success, Try}


object TryImplicits {

  implicit class TryOps[A](private val value: Try[A]) extends AnyVal {

    /**
      * Sends a Try as a response inside an Actor, to the sender in the unwrapped form
      */
    def answerTo(sender: ActorRef): Unit = {
      value match {
        case Success(value) => sender ! value
        case Failure(error) => sender ! Status.Failure(error)
      }
    }
  }

}
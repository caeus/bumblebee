package edu.caeus.bumblebee.util

/**
  * Runs a set of validations on an object and collects validation messages
  * Kind of similar to cats.Validate, but I didn't want to use cats for this project
  * It'd be an overkill
  * @param checks
  * @tparam A
  */
class Check[A](checks: List[A => Option[String]]) {
  def and(check: PartialFunction[A, String]): Check[A] =
    new Check[A](check.lift :: checks)

  def on(obj: A): List[String] = checks.flatMap(_(obj))
}

object Check {
  def apply[A](check: PartialFunction[A, String]): Check[A] =
    new Check(List(check.lift))
}

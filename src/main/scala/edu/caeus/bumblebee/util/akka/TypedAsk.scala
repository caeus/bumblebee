package edu.caeus.bumblebee.util.akka

import akka.actor.{Actor, ActorRef}
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.Future
import scala.reflect.ClassTag


/**
  * Given I don't like the type unsafety that appears when we use Actors, I try to wrap unsafe calls
  *
  * @tparam T
  */
trait TypedAsk[T] {

  def askTo(ref: ActorRef)(implicit timeout: Timeout,
                           sender: ActorRef = Actor.noSender,
                           classTag: ClassTag[T]): Future[T] = {
    (ref ? this).mapTo[T]
  }

}

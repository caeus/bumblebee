package edu.caeus.bumblebee.engines

import java.net.URLEncoder
import java.time.temporal.ChronoUnit
import java.time.{Clock, Instant}

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.pattern.{ask, pipe}
import akka.util.Timeout
import edu.caeus.bumblebee.engines.ChargeSessionsActor.{Add, All}
import edu.caeus.bumblebee.errors.InvalidDataException
import edu.caeus.bumblebee.models.{ChargeSession, ChargeSessionSeed, Price, Tariff}
import edu.caeus.bumblebee.util.Check
import edu.caeus.bumblebee.util.syntax.TryImplicits._
import edu.caeus.bumblebee.util.akka.TypedAsk

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

/**
  * Service in charge of managing Charging sessions
  *
  * @param system
  * @param clock
  * @param tariffsEngine
  * @param executionContext
  * @param timeout
  */
class ChargeSessionsEngine(system: ActorSystem,
                           clock: Clock,
                           tariffsEngine: TariffsEngine)
                          (implicit executionContext: ExecutionContext,
                           timeout: Timeout) {

  private val underlyingActor = system.actorOf(ChargeSessionsActor.props)


  private def calculatePrice(seed: ChargeSessionSeed, tariff: Tariff): Seq[Price] = {
    val hours = seed.startTime.until(seed.endTime, ChronoUnit.SECONDS) / 3600.0
    List(tariff.hourlyFee * hours,
      tariff.startFee,
      tariff.feePerKWh * seed.volume
    ).groupBy(_.currency).map {
      case (currency, prices) =>
        Price(value = prices.map(_.value).sum, currency)
    }.toList.filter(_.value > 0)
  }


  private val checker: Check[ChargeSessionSeed] = Check[ChargeSessionSeed] {
    case seed if seed.endTime.toInstant.isAfter(Instant.now(clock)) =>
      /**
        * As Tariffs cannot be retroactive, if we allow sessions to be registered after last tariff
        * It will cause Tariffs to become retroactive, so I'll add this validation:
        * No sessions with starting or ending time that are after NOW.
        * A check could be added so that they are not added after last tariff, but in terms of how the real world
        * works, it doesn't make any sense to have sessions in the future
        *
        */
      "Cannot register a session with end date in the future"
  }.and {
    case seed if seed.endTime.isBefore(seed.startTime) =>
      "End time must be after Start time"
  }


  def add(seed: ChargeSessionSeed): Future[ChargeSession] = {


    checker.on(seed) match {
      case Nil =>
        tariffsEngine.activeAt(seed.startTime.toInstant)
          .flatMap {
            tariff =>
              Add(ChargeSession(customerId = seed.customerId: String,
                startTime = seed.startTime,
                endTime = seed.endTime,
                volume = seed.volume,
                price = calculatePrice(seed, tariff)
              )).askTo(underlyingActor)
          }
      case errors =>
        Future.failed(InvalidDataException("Illegal or inconsistent data", errors))
    }


  }

  def all(id: String): Future[Seq[ChargeSession]] = All(id).askTo(underlyingActor)

}


class ChargeSessionActorByCustomer(id: String) extends Actor {

  //Order list for every time a session is added
  var sessions: List[ChargeSession] = Nil


  def add(session: ChargeSession): Try[ChargeSession] = {
    //
    Try {
      //Partition the thing between the recent and older sessions
      val (recent, older) = sessions.partition(_.startTime.isAfter(session.startTime))
      //Then I put the new session in between...
      // This will assure I'll have always an ordered list
      sessions = recent ::: session :: older
      session
    }
  }


  override def receive: Receive = {
    case Add(session) =>
      add(session).answerTo(sender())
    case All(_) =>
      sender() ! sessions
  }
}

object ChargeSessionActorByCustomer {
  def props(id: String) = Props(new ChargeSessionActorByCustomer(id))
}

/**
  * Mocks the Database, as a real one won't be used for the test
  * I'm not really a fan of Actors (because I literally dodge all the scala typesafety)
  * But I think that they are good for two specific use cases
  * 1. Isolate mutable state managment
  * 2. Fine concurrency management
  */
class ChargeSessionsActor(implicit timeout: Timeout) extends Actor {

  import context.dispatcher

  def byCustomerId(id: String): ActorRef = {
    val safeId = URLEncoder.encode(id, "UTF-8")
    context.child(safeId)
      .getOrElse(context.actorOf(ChargeSessionActorByCustomer.props(id), safeId))
  }

  override def receive: Receive = {
    case cmd@Add(seed) =>
      (byCustomerId(seed.customerId) ? cmd).pipeTo(sender())
    case cmd@All(id) =>
      cmd.askTo(byCustomerId(id))
        .pipeTo(sender())
  }
}

object ChargeSessionsActor {

  sealed trait Op[A] extends TypedAsk[A]

  case class Add(seed: ChargeSession) extends Op[ChargeSession]

  case class All(id: String) extends Op[Seq[ChargeSession]]

  def props(implicit timeout: Timeout) = Props(new ChargeSessionsActor)
}

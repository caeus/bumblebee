package edu.caeus.bumblebee.engines

import java.time.{Clock, Instant, ZonedDateTime}
import java.util.Currency

import akka.actor.{Actor, ActorSystem, Props}
import akka.util.Timeout
import edu.caeus.bumblebee.errors.InvalidDataException
import edu.caeus.bumblebee.models.{Price, Tariff, TariffSeed}
import edu.caeus.bumblebee.util.Check
import edu.caeus.bumblebee.util.akka.TypedAsk
import edu.caeus.bumblebee.util.syntax.TryImplicits
import ujson.Js.InvalidData

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

/**
  * Service in charge of managing Tariffs
  *
  * @param system
  * @param clock
  * @param timeout
  */
class TariffsEngine(system: ActorSystem, clock: Clock)(implicit timeout: Timeout) {

  private val underlyingActor = system.actorOf(TariffsActor.props(clock))

  private val checker = Check[TariffSeed] {
    case seed if List(seed.feePerKWh, seed.hourlyFee, seed.startFee)
      .forall(_.isEmpty) =>
      "At least one of the fees should be set"
  }.and {
    case seed if seed.activeStarting.toInstant.isBefore(clock.instant()) =>
      "New tariff cannot apply before now"
  }


  def set(seed: TariffSeed): Future[Tariff] = {
    checker.on(seed) match {
      case Nil =>
        TariffsActor.Set(seed).askTo(underlyingActor)
      case errors =>
        Future.failed(InvalidDataException("Wrong data", errors))
    }
  }


  def activeAt(time: Instant): Future[Tariff] = {
    TariffsActor.ActiveAt(time).askTo(underlyingActor)
  }

}

class TariffsActor(clock: Clock) extends Actor {

  import TryImplicits._

  var tariffs: List[Tariff] = {
    val eur = Currency.getInstance("EUR")
    List(Tariff(
      startFee = Price(0, eur),
      hourlyFee = Price(0, eur),
      feePerKWh = Price(0, eur),
      activeSince = ZonedDateTime.ofInstant(Instant.EPOCH, clock.getZone)))
  }


  def set(seed: TariffSeed): Try[Tariff] = {
    tariffs match {
      case last :: _ if last.activeSince.isBefore(seed.activeStarting) =>
        val updated = last.update(seed)
        tariffs = updated :: tariffs
        Success(updated)
      case last :: _ =>
        Failure(InvalidDataException(s"Tariff cannot be retroactive, it should be active after ${last.activeSince}"))
      case _ => Failure(new IllegalStateException("Empty tariffs table... please report this but with VenerableInertia"))
    }
  }

  def activeAt(time: Instant): Try[Tariff] = {
    // Find method is enough, if you think about it, it will provide the first Tariff before time
    tariffs.find(_.activeSince.toInstant.isBefore(time)) match {
      case Some(tariff) => Success(tariff)
      case None => Failure(InvalidDataException("Sessions before Epoch are not processed"))
    }
  }

  override def receive: Receive = {
    case TariffsActor.Set(seed) =>
      set(seed).answerTo(sender())
    case TariffsActor.ActiveAt(time) =>
      activeAt(time).answerTo(sender())

  }
}

object TariffsActor {

  sealed trait Op[A] extends TypedAsk[A]

  case class Set(seed: TariffSeed) extends Op[Tariff]

  case class ActiveAt(time: Instant) extends Op[Tariff]

  def props(clock: Clock) = Props(new TariffsActor(clock))

}

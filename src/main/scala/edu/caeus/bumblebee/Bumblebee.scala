package edu.caeus.bumblebee

import java.time.Clock

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.{ExceptionHandler, Route}
import akka.stream.ActorMaterializer
import akka.util.Timeout
import edu.caeus.bumblebee.engines.{ChargeSessionsEngine, TariffsEngine}
import edu.caeus.bumblebee.routes.Routes

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._

/**
  * I like the concept of dependency injection, so with this class I'm somehow mocking a DIModule
  * Manually wiring dependencies. Idea copied from the Macwire library
  */
class Bumblebee() {

  lazy implicit val system: ActorSystem = ActorSystem("bumblebee")
  lazy implicit val materializer: ActorMaterializer = ActorMaterializer()
  lazy implicit val executionContext: ExecutionContext = system.dispatcher

  lazy implicit val askTimeout: Timeout = Timeout(10.seconds)

  lazy val clock: Clock = Clock.systemDefaultZone

  lazy val tariffsEngine: TariffsEngine = new TariffsEngine(system, clock)

  lazy val chargeSessionsEngine: ChargeSessionsEngine = new ChargeSessionsEngine(system, clock, tariffsEngine)

  lazy implicit val exceptionHandler: ExceptionHandler = Routes.exceptionHandler

  lazy val routes: Route = Routes(tariffsEngine, chargeSessionsEngine)

  /**
    *
    */
  lazy val httpBinding: Future[Http.ServerBinding] =
    Http().bindAndHandle(
      handler = routes,
      interface = "0.0.0.0",
      port = 8000)


  // Manually started by evaluating lazy httpBinding
  def start(): Unit = httpBinding
}

object Bumblebee {


  def main(args: Array[String]): Unit = {
    new Bumblebee().start()
  }

}

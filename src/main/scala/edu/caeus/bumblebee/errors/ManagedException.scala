package edu.caeus.bumblebee.errors

import akka.http.scaladsl.model.{HttpResponse, StatusCode}
import edu.caeus.bumblebee.util.json.Codecs._

trait ManagedException {
  this: Exception =>

  val httpStatus: StatusCode

  val message: String

  val details: Seq[String]

}

case class ErrorMessage(message: String, details: Seq[String])

object ErrorMessage {
  implicit def readwriter: ReadWriter[ErrorMessage] = macroRW
}


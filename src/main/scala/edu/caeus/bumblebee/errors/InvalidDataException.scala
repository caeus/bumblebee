package edu.caeus.bumblebee.errors

import akka.http.scaladsl.model.{StatusCode, StatusCodes}

case class InvalidDataException(message: String, details:Seq[String]=Nil)
  extends IllegalArgumentException(message) with ManagedException {
  override val httpStatus: StatusCode = StatusCodes.BadRequest
}

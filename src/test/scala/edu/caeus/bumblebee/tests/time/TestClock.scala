package edu.caeus.bumblebee.tests.time

import java.time.{Clock, Instant, ZoneId}
import java.util.concurrent.atomic.AtomicLong

import scala.concurrent.duration.FiniteDuration


/**
  * A clock that I can use to test feature that depend on current time.
  * It contains an AtomicLong that I can modify via the method forward.
  * It allows me to travel back or forward in time in order to add sessions in different times.
  * @param offset
  * @param underlyingClock
  */
class TestClock(offset: AtomicLong = new AtomicLong(0l),
                private val underlyingClock: Clock = Clock.systemDefaultZone()) extends Clock {


  override def getZone: ZoneId = underlyingClock.getZone

  override def withZone(zone: ZoneId): Clock = new TestClock(offset, underlyingClock.withZone(zone))

  override def instant(): Instant = underlyingClock.instant().plusMillis(offset.get())

  def forward(duration: FiniteDuration): Long = offset.updateAndGet(_ + duration.toMillis)
}

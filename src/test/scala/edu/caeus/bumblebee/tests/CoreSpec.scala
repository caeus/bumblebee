package edu.caeus.bumblebee.tests

import java.time.temporal.ChronoUnit
import java.time.{Clock, Instant, LocalDate, ZonedDateTime}
import java.util.Currency
import java.util.concurrent.atomic.AtomicLong

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.{RouteTest, TestFrameworkInterface}
import edu.caeus.bumblebee.Bumblebee
import edu.caeus.bumblebee.errors.ManagedException
import edu.caeus.bumblebee.models.{ChargeSessionSeed, TariffSeed}
import edu.caeus.bumblebee.tests.time.TestClock
import utest._

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

object CoreSpec extends TestSuite {

  implicit class FutureOps[A](private val value: Future[A]) extends AnyVal {
    def await: A = Await.result(value, 10.seconds)
  }

  val testClock = new TestClock()
  val main: Bumblebee = new Bumblebee {
    override lazy val clock: Clock = testClock
  }

  val tests = Tests {

    " Tariffs Engine" - {

      val tariffsEngine = main.tariffsEngine

      " Invalid seed (no fees)" - {
        intercept[ManagedException](tariffsEngine.set(TariffSeed(currency = Currency.getInstance("EUR"),
          startFee = None,
          hourlyFee = None,
          feePerKWh = None,
          ZonedDateTime.now(testClock))).await
        )
      }
      " Invalid seed (before now)" - {
        intercept[ManagedException](tariffsEngine.set(TariffSeed(currency = Currency.getInstance("EUR"),
          startFee = Some(1.2),
          hourlyFee = None,
          feePerKWh = None,
          ZonedDateTime.now(testClock).minusDays(1))).await
        )
      }
      " Valid seed (USD)" - {
        tariffsEngine.set(TariffSeed(currency = Currency.getInstance("USD"),
          startFee = Some(1.2),
          hourlyFee = None,
          feePerKWh = None,
          ZonedDateTime.now(testClock).plusDays(1))).await
        locally {
          val tariff = tariffsEngine.activeAt(Instant.now(testClock).plus(2, ChronoUnit.DAYS)).await
          tariff.startFee.value ==> 1.2
          tariff.startFee.currency.getCurrencyCode ==> "USD"
        }
        locally {
          val tariff = tariffsEngine.activeAt(Instant.now()).await
          tariff.startFee.value ==> 0.0
        }
      }
      " Invalid seed (before last)" - {
        intercept[ManagedException](tariffsEngine.set(TariffSeed(currency = Currency.getInstance("EUR"),
          startFee = Some(1.5),
          hourlyFee = None,
          feePerKWh = None,
          ZonedDateTime.now(testClock).plusHours(12))).await)

      }
      " Valid seed (EUR)" - {
        tariffsEngine.set(TariffSeed(currency = Currency.getInstance("EUR"),
          startFee = None,
          hourlyFee = Some(1.4),
          feePerKWh = Some(4),
          ZonedDateTime.now(testClock).plusDays(2))).await
        locally {
          val tariff = tariffsEngine.activeAt(Instant.now(testClock).plus(3, ChronoUnit.DAYS)).await
          tariff.startFee.value ==> 1.2
          tariff.startFee.currency.getCurrencyCode ==> "USD"
          tariff.hourlyFee.value ==> 1.4
          tariff.hourlyFee.currency.getCurrencyCode ==> "EUR"
        }
      }
    }

    " Charge Sessions Engine" - {

      val sessionsEngine = main.chargeSessionsEngine

      " Invalid (in the future)" - {
        intercept[Exception] {
          sessionsEngine.add(ChargeSessionSeed(customerId = "caeus",
            startTime = ZonedDateTime.now(testClock).plusHours(1),
            endTime = ZonedDateTime.now(testClock).plusHours(3),
            volume = 10.0)).await
        }
      }
      " Invalid (start after end)" - {
        intercept[Exception] {
          sessionsEngine.add(ChargeSessionSeed(customerId = "caeus",
            startTime = ZonedDateTime.now(testClock).plusHours(3),
            endTime = ZonedDateTime.now(testClock).plusHours(1),
            volume = 10.0)).await
        }
      }
      " Valid" - {
        testClock.forward(1.day + 4.hour)

        val session = sessionsEngine.add(ChargeSessionSeed(customerId = "caeus",
          startTime = ZonedDateTime.now(testClock).minusHours(3),
          endTime = ZonedDateTime.now(testClock).minusHours(1),
          volume = 10.0)).await
        val sessions = sessionsEngine.all("caeus").await

        // So far only one session registered for user caeus
        session ==> sessions.head

        // At this point only USD prices have been set
        session.price.size ==> 1

        // Only applying fee is startingFee
        session.price.head.value ==> 1.2

      }
      " Valid (with currency change)" - {
        testClock.forward(1.day)

        val session = sessionsEngine.add(ChargeSessionSeed(customerId = "caeus",
          startTime = ZonedDateTime.now(testClock).minusHours(3),
          endTime = ZonedDateTime.now(testClock).minusHours(1),
          volume = 10.0: Double)).await

        val sessions = sessionsEngine.all("caeus").await

        session ==> sessions.head //Last session must be recent session

        session.price.size ==> 2 // EUR and USD

        // Hourly fee of 1.4 during 2 hours, plus feePerKWh of 4 for 10KWh
        session.price.find(_.currency.getCurrencyCode == "EUR").get.value ==> 2 * 1.4 + 4 * 10

        // Start fee hasn't changed
        session.price.find(_.currency.getCurrencyCode == "USD").get.value ==> 1.2

      }
      " Valid (new customer with weird name)" - {
        val customerId = "66qwe y&"
        val session = sessionsEngine.add(ChargeSessionSeed(customerId = customerId,
          startTime = ZonedDateTime.now(testClock).minusHours(3),
          endTime = ZonedDateTime.now(testClock).minusHours(1),
          volume = 10.0)).await
        sessionsEngine.all(customerId).await.size ==> 1
      }
      " Report for unexisting customer" - {
        sessionsEngine.all("unexisting").await.size ==> 0
      }
    }


  }


}

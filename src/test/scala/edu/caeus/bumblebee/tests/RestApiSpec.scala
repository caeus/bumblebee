package edu.caeus.bumblebee.tests

import java.time.ZonedDateTime

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.{RouteTest, TestFrameworkInterface}
import edu.caeus.bumblebee.Bumblebee
import edu.caeus.bumblebee.errors.ErrorMessage
import edu.caeus.bumblebee.models.{ChargeSession, Tariff}
import edu.caeus.bumblebee.util.akka.UpickleSupport._
import edu.caeus.bumblebee.util.json.Codecs._
import ujson.Js
import utest._

object RestApiSpec extends TestSuite with RouteTest with TestFrameworkInterface {
  val main = new Bumblebee()


  val sealedRoutes = {
    import main.exceptionHandler
    Route.seal(main.routes)
  }
  //main.start()

  val tests = Tests {

    " Invalid POST v1/tariffs (wrong content type)" - {

      Post("/v1/tariffs", "text content") ~> sealedRoutes ~> check {
        status ==> StatusCodes.BadRequest
      }

    }
    " Invalid POST v1/tariffs (wrong json)" - {


      Post("/v1/tariffs", Js.Obj(
        "currency" -> Js.Str("EUR")
      )) ~> sealedRoutes ~> check {
        status ==> StatusCodes.BadRequest
      }
    }

    " Invalid POST v1/tariffs (wrong currency)" - {
      Post("/v1/tariffs", Js.Obj(
        "currency" -> Js.Str("AAA"),
        "activeStarting" -> Js.Str(ZonedDateTime.now().toString)
      )) ~> sealedRoutes ~> check {
        status ==> StatusCodes.BadRequest
      }
    }

    " Invalid POST v1/tariffs (wrong currency)" - {
      Post("/v1/tariffs", Js.Obj(
        "currency" -> Js.Str("AAA"),
        "activeStarting" -> Js.Str(ZonedDateTime.now().toString)
      )) ~> sealedRoutes ~> check {
        status ==> StatusCodes.BadRequest
      }
    }

    " Invalid POST v1/tariffs (business requirement, no fees)" - {
      Post("/v1/tariffs", Js.Obj(
        "currency" -> Js.Str("EUR"),
        "activeStarting" -> Js.Str(ZonedDateTime.now().minusHours(1).toString)
      )) ~> sealedRoutes ~> check {
        status ==> StatusCodes.BadRequest
        responseAs[ErrorMessage].details.size ==> 2
      }
    }


    " Valid POST v1/tariffs" - {
      Post("/v1/tariffs", Js.Obj(
        "currency" -> Js.Str("EUR"),
        "hourlyFee" -> Js.Num(4),
        "activeStarting" -> Js.Str(ZonedDateTime.now().plusHours(1).toString)
      )) ~> sealedRoutes ~> check {
        status ==> StatusCodes.OK
        responseAs[Tariff]
      }
    }


    " Invalid POST v1/charge-sessions (wrong content type)" - {
      Post("/v1/charge-sessions", "asd") ~> sealedRoutes ~> check {
        status ==> StatusCodes.BadRequest
      }
    }

    " Invalid POST v1/charge-sessions (wrong json)" - {
      Post("/v1/charge-sessions", Js.Obj("Hey" -> Js.Str("Jude"))) ~> sealedRoutes ~> check {
        status ==> StatusCodes.BadRequest
      }
    }

    " Invalid POST v1/charge-sessions (invalid data, business requirement)" - {

      Post("/v1/charge-sessions", Js.Obj(
        "customerId" -> Js.Str("Jude"),
        "startTime" -> Js.Str(ZonedDateTime.now().toString),
        "endTime" -> Js.Str(ZonedDateTime.now().minusHours(1).toString),
        "volume" -> Js.Num(25.6)
      )) ~> sealedRoutes ~> check {
        status ==> StatusCodes.BadRequest
        responseAs[ErrorMessage]
      }

      " Valid POST v1/charge-sessions" - {
        Post("/v1/charge-sessions", Js.Obj(
          "customerId" -> Js.Str("Jude"),
          "startTime" -> Js.Str(ZonedDateTime.now().minusHours(1).toString),
          "endTime" -> Js.Str(ZonedDateTime.now().toString),
          "volume" -> Js.Num(25.6)
        )) ~> sealedRoutes ~> check {
          status ==> StatusCodes.OK
          responseAs[ChargeSession]
        }
      }

      " Valid GET v1/charge-sessions/:customerId" - {
        Get("/v1/charge-sessions/Jude") ~> sealedRoutes ~> check {
          status ==> StatusCodes.OK
          responseAs[Seq[ChargeSession]].size ==> 1
        }
      }
    }
  }

  override def failTest(msg: String): Nothing =
  //Knowing I had to do this implied a lot of reading regarding how scala testing works
    throw new java.lang.AssertionError(msg)

}

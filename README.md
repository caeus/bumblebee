#### Installation
I assume `git` and `sbt` are already installed, so:

`git clone git@bitbucket.org:caeus/bumblebee.git`

`cd bumblebee`

#### Testing
Tests are divided in two big sets:
- A suite that tests the logic, agnostic to akka http
- A suite that tests the rest api

You can run the tests by executing this:

`sbt test`

#### Running
The service is hardwired to listen in `http://localhost:8000`.
You can start it by running:

`sbt run` 

#### Thanks
I really hope you like my solution, looking forward to your feedback.



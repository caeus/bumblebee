lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "edu.caeus",
      scalaVersion := "2.12.4",
      version := "0.1.0-SNAPSHOT"
    )),
    name := "bumblebee",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http" % "10.0.11",
      "com.lihaoyi" %% "upickle" % "0.6.5",
      "com.lihaoyi" %% "utest" % "0.6.0" % Test,
      "com.typesafe.akka" %% "akka-http-testkit" % "10.0.11" % Test),
    testFrameworks := Seq(new TestFramework("utest.runner.Framework"))
  )
